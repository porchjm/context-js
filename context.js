// Wraps a type and a message
// Useful for when code needs to know the kind of response (error, etc) but we want details for the humans
class Message {
  constructor(type, message) {
    this.type = type;
    this.message = message;
  }

  static missingKeyError(method, key) {
    return new Message('error', `[${method}]: State does not contain value ${key}`)
  }

  static duplicateKeyError(method, key) {
    return new Message('error', `[${method}]: State already contains value ${key}`)
  }

  static noChange(method, key, value) {
    return new Message('noChange',`[${method}]: State "${key}" was already ${value}`);
  }

  static transactionInProgressError(method) {
    return new Message('error', `[${method}] Transaction already in progress`);
  }

  static transactionNotInProgressError(method) {
    return new Message('error', `[${method}] No transaction currently in progress`);
  }

  static transactionSuccess(method) {
    return new Message('success', `[${method}] Transaction operation completed`);
  }
}

// Returned from an updateState call, 
class Results {
  constructor(operation, args, status, results) {
    this.operation = operation;
    this.args = args;
    this.status = status?'success':'error';
    this.results = results;
  }
}

// Wraps all state changes
class Tuple {
  constructor(key, value) {
    this.key = key;
    this.value = value;
  }
}

// Specifies an array of keys to watch for changes and an effect to fire if any of them change.
// Can be registered to any Context and it will watch keys within that specific context
class Hook {
  constructor(watch=[], effect) {
    this.watch = watch;
    this.effect = effect;
  }
}

class Context {
  constructor () {
    this.state = {};
    this.hooks = [];

    this.backupState = null;
    this.transaction = false;
  }

  contains(key) {
    return this.state.hasOwnProperty(key);
  }

  addState(key, value=null) {
    if (this.contains(key)) return Message.duplicateKeyError('addState',key);
    
    this.state[key] = value;
    return new Message('success', `[addState]: Added state "${key}", initial value of ${value}`);
  }

  getValue(key) {
    if (!this.contains(key)) return null;
    return this.state[key];
  }

  removeState(key) {
    if (!this.contains(key)) return Message.missingKeyError('removeState', key);

    delete this.state[key];
    return new Message('success', `[removeState]: Removed state "${key}"`);
  }

  updateValue(key, newValue, state=this.state) {
    if (!this.contains(key)) return Message.missingKeyError('updateValue', key);
    if (state[key] === newValue) return Message.noChange('updateValue',key,newValue);

    state[key] = newValue;
    return new Message('success', `[updateValue]: Updated state "${key}" to ${newValue}`);
  }

  addHook(hook) {
    this.hooks.push(hook);
  }

  removeHook() {
    // TODO: figure out how this even makes sense?
    // Like, probably have to remove by index but then how do you get that?
    // Could refactor to have hook with IDs, but that means changing code like the foreach that checks hooks
  }


  // Accepts a single Tuple or an array of Tuples
  updateState(updates) {
    if (updates.constructor.name === 'Tuple') {
      return this.updateValue(updates.key, updates.value);
    } else if (updates.constructor.name === 'Array') {
      // Object.assign is a shallow copy, can use JSON stringify+parse if I need a deep clone
      // TODO: Probably want to do deep clone so that state can hold objects
      const newState = Object.assign({}, this.state);
      const results = [];
      const changes = {};
      let error = false;

      // Applies all changes, saving results of each.
      // Errors trip a flag, successful changes add to a list
      updates.forEach(element => {
        results.push(this.updateValue(element.key, element.value, newState));
        if (results.slice(-1)[0].type === 'error') {
          error = true;
        }
        else if (!(results.slice(-1)[0].type === 'noChange')) {
          changes[element.key] = true;
        }
      });

      // Only accept the new state if all were successful (atomic updates)
      // TODO: Error handling while hooks fire
      if (!error && Object.keys(changes).length > 0) {
        this.hooks.forEach(hook => {
          // If the list of changes contains any value in the hooks watchlist, fire the hook's effect
          // Note: will also fire any hooks that don't provide any specific items to watch
          // TODO: Think about implications of this WRT circular changes (if a hook can change state, can it trigger itself?)
          if (hook.watch.length === 0 || hook.watch.some(e=> changes.hasOwnProperty(e))) results.push(hook.effect(newState));
        });

        // TODO: Formal constraints?
        // In addition to hooks that fire on change, we could allow for a final pass of methods that run against the whole state and give pass/fail

        // If we've made it this far with no errors, then we accept the new state
        if (!error) this.state = newState;
      }
      return new Results('updateState', updates, !error, results);
    }
  }

  // TODO: Make this use deep clones
  transactionStart() {
    if (this.transaction) return Message.transactionInProgressError('transactionStart');
    this.backupState = Object.assign({}, this.state);
    this.transaction = true;
    return Message.transactionSuccess('transactionStart');
  }
  transactionRollback() {
    if (!this.transaction) return Message.transactionNotInProgressError('transactionRollback');
    this.state = this.backupState;
    this.backupState = null;
    this.transaction = false;
    return Message.transactionSuccess('transactionRollback');
  }
  transactionCommit() {
    if (!this.transaction) return Message.transactionNotInProgressError('transactionCommit');
    this.backupState = null;
    this.transaction = false;
    return Message.transactionSuccess('transactionCommit');
  }
}

// Go ahead and give a default Context that all code can use
const GlobalContext = new Context();

