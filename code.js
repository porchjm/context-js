GlobalContext.addState("test",1);
GlobalContext.updateValue("test",2);
GlobalContext.updateState(new Tuple("test",3));
GlobalContext.addState("foo",1);
GlobalContext.addState("bar","a");
GlobalContext.addHook(new Hook([], 
  (s)=> {
    const msg = `[nada]: nil`
    console.log(msg);
    return new Message('success',msg);
}));

GlobalContext.addHook(new Hook(['foo'], 
  (s)=> {
    const msg = `[foo]: ${s['foo']}`
    console.log(msg);
    return new Message('success',msg);
}));

GlobalContext.addHook(new Hook(['foo','bar'], 
(s)=> {
  const msg = `[foobar]: ${s['foo']}${s['bar']}`
  console.log(msg);
  return new Message('success',msg);
}));

GlobalContext.addHook(new Hook(['bar','baz'], 
(s)=> {
  const msg = `[barbaz]: ${s['bar']}${s['baz']}`
  console.log(msg);
  return new Message('success',msg);
}));
console.log(GlobalContext.transactionStart());
console.log(GlobalContext.updateState([
  new Tuple("foo",2),
  new Tuple("bar","a"),
]));
console.log(GlobalContext.transactionRollback());

console.log(GlobalContext.transactionStart());
console.log(GlobalContext.transactionStart());
console.log(GlobalContext.updateState([
  new Tuple("foo",2),
  new Tuple("bar","c"),
  new Tuple("baz", -1)
]));
console.log(GlobalContext.transactionCommit());

console.log(GlobalContext);